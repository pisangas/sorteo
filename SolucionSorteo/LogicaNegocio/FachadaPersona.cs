﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaPersona
    {
        public DBSorteoEntities _contexto;
        public FachadaPersona()
        {
            _contexto = new DBSorteoEntities();
        }
        public int InsertarPersona(Persona persona)
        {
            try
            {
                _contexto.Personas.Add(persona);
                return _contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int ActualizarPersona(Persona persona)
        {
            try
            {
                _contexto.Entry(persona).State = System.Data.Entity.EntityState.Modified;
                return _contexto.SaveChanges();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public Persona RetornarPersona(int cedula)
        {
            try
            {
                return _contexto.Personas.Find(cedula);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Persona> RetornarPersonas()
        {
            try
            {
                return _contexto.Personas.OrderBy(p => p.Nombres).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int EliminarPersona(int cedula)
        {
            try
            {
                Persona persona = _contexto.Personas.Find(cedula);

                if (persona != null)
                {
                    _contexto.Personas.Remove(persona);
                    return _contexto.SaveChanges();
                }
                return 0;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

    }
}
