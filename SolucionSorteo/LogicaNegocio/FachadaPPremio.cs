﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace LogicaNegocio
{
    public class FachadaPPremio
    {
        public DBSorteoEntities _contexto;
        public FachadaPPremio()
        {
            _contexto = new DBSorteoEntities();
        }
        public List<PPremio> RetornarPremios()
        {
            try
            {
                return _contexto.PPremios.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }        
        public int InsertarPremio(PPremio pPremio)
        {
            try
            {
                _contexto.PPremios.Add(pPremio);
                return _contexto.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
