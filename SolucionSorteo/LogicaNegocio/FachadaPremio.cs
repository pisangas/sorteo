﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaPremio
    {
        public DBSorteoEntities _contexto;
        Random random = new Random();
        private int registro = 0;
        public FachadaPremio()
        {
            _contexto = new DBSorteoEntities();
        }
        public List<Premio> RetornarPremio()
        {
            try
            {
                registro = random.Next(1, 4);

                var existenciasPremios = (from p in _contexto.Premios
                                          where p.Cantidad > 0
                                          select p).ToList();

                var existenciaPremio = (from p in _contexto.Premios
                                        where p.Codigo == registro
                                        where p.Cantidad > 0
                                        select p).ToList();

                var restaPremio = (from p in _contexto.Premios
                                   where p.Codigo == registro
                                   select p).FirstOrDefault();

                restaPremio.Cantidad -= 1;
                _contexto.SaveChanges();

                if (existenciasPremios.ToList().Count == 0)
                {
                    throw new Exception("No hay premios disponibles, el sorteo no estara disponibles");
                }
                return existenciaPremio.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}