﻿using AccesoDatos;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class WebFormGenerarSorteo : System.Web.UI.Page
    {
        #region [Variables privadas]
        private FachadaPPremio _fachadaPPremios = new FachadaPPremio();
        private FachadaPremio _fachadaPremios = new FachadaPremio();
        private FachadaPersona _fachadaPersona = new FachadaPersona();
        #endregion

        #region [Metodos privados]
        private void CargarPremios()
        {
            gvPPremios.DataSource = _fachadaPPremios.RetornarPremios();
            gvPPremios.DataBind();
        }
        private void CargarSorteo()
        {
            try
            {
                List<Persona> personas = _fachadaPersona.RetornarPersonas();
                PPremio pPremio = new PPremio();

                foreach (var item in personas)
                {
                    pPremio.Cantidad = 1;
                    pPremio.IdPersona = item.NroDocumento;
                    pPremio.Fecha = DateTime.Now;

                    List<Premio> premios = _fachadaPremios.RetornarPremio();
                    if (premios.Count == 0)
                    {
                        lbResultado.CssClass = "text-danger";
                        lbResultado.Text = "Se agotan los premios";
                        CargarPremios();
                    }
                    else
                    {
                        foreach (var item2 in premios)
                        {
                            pPremio.IdPremio = item2.Codigo;
                        }

                        _fachadaPPremios.InsertarPremio(pPremio);
                        CargarPremios();
                    }
                }
            }
            catch (Exception ex)
            {
                lbResultado.CssClass = "text-danger";
                lbResultado.Text = ex.Message;
                btnGenerarSorteo.Enabled = false;
            }

        }
        protected void btnGenerarSorteo_Click(object sender, EventArgs e)
        {
            CargarSorteo();
        }
        #endregion

        #region [Eventos de pagina]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CargarPremios();
            }
        }
        protected void gvPPremios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPPremios.PageIndex = e.NewPageIndex;
            gvPPremios.DataSource = _fachadaPPremios.RetornarPremios();
            gvPPremios.DataBind();
        }
        #endregion
    }
}