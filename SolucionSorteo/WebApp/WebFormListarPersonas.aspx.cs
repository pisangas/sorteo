﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class WebFormListarPersonas : System.Web.UI.Page
    {
        #region [Variables privadas]
        private FachadaPersona _fachadaPersona = new FachadaPersona();
        #endregion

        #region [Metodos Privados]
        private void CargarPersonas()
        {
            gvPersonas.DataSource = _fachadaPersona.RetornarPersonas();
            gvPersonas.DataBind();
        }
        #endregion

        #region [Eventos de la pagina]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CargarPersonas();
            }
        }
        protected void gvPersonas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Eliminar")
                {
                    int cedula = Convert.ToInt32(e.CommandArgument);

                    if (_fachadaPersona.EliminarPersona(cedula) == 1)
                    {
                        CargarPersonas();
                        lbResultado.CssClass = "text-sucess";
                        lbResultado.Text = $"La persona se elimino correctamente({DateTime.Now.ToString()})";
                    }
                    else
                    {
                        lbResultado.CssClass = "text-danger";
                        lbResultado.Text = $"La persona no se pudo eliminar ({DateTime.Now.ToString()})";
                    }
                }
            }
            catch (Exception ex)
            {
                lbResultado.CssClass = "text-danger";
                lbResultado.Text = $"La persona no se pudo eliminar ya que tiene premios asociados ({DateTime.Now.ToString()} {ex.Message})";
            }

        }
        protected void gvPersonas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPersonas.PageIndex = e.NewPageIndex;
            gvPersonas.DataSource = _fachadaPersona.RetornarPersonas();
            gvPersonas.DataBind();
        }
        #endregion
    }
}