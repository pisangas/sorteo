﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormListarPersonas.aspx.cs" Inherits="WebApp.WebFormListarPersonas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Content/paginacion.css" rel="stylesheet" />
    <asp:GridView ID="gvPersonas" runat="server" CssClass="table table-striped table-dark table-hover mt-3" AutoGenerateColumns="false" AllowPaging="true" PageSize="5" OnPageIndexChanging="gvPersonas_PageIndexChanging" OnRowCommand="gvPersonas_RowCommand">
        <Columns>
            <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoCedula" />
            <asp:HyperLinkField DataTextField="NroDocumento" HeaderText="Numero Identificación" DataNavigateUrlFormatString="/WebFormPersona.aspx?NroDocumento={0}" DataNavigateUrlFields="NroDocumento" />
            <asp:BoundField HeaderText="Nombres" DataField="Nombres" />
            <asp:BoundField HeaderText="Apellidos" DataField="Apellidos" />
            <asp:BoundField HeaderText="Feha de Nacimiento" DataField="F_Nacimiento" />
            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                <ItemTemplate>
                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger" OnClientClick="return confirm('¿Realmente desea eliminar esta persona?');" CommandName="Eliminar" CommandArgument='<%# Bind("NroDocumento") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:Label ID="lbResultado" runat="server" Text=""></asp:Label>


</asp:Content>
