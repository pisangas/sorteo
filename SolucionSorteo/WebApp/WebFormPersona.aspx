﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormPersona.aspx.cs" Inherits="WebApp.WebFormPersona" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card-header text-white badge-dark">
           Persona
        </div>
        <div class="card-body">            
            <div class="form-group row">
                <label class="col-md-2 col-form-label">Nombres:</label>
                <div class="col-md-10">
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" PlaceHolder="Ingrese Nombres"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="El nombre es requerido" ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger"></asp:RequiredFieldValidator>                    
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 col-form-label">Apellidos:</label>
                <div class="col-md-10">
                    <asp:TextBox ID="txtApellidos" runat="server" CssClass="form-control" PlaceHolder="Ingrese Apellidos"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvApellidos" runat="server" ErrorMessage="Los apellidos son requeridos" ControlToValidate="txtApellidos" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger"></asp:RequiredFieldValidator>                    
                </div>
            </div>           
            <div class="form-group row">
                <label class="col-md-2 col-form-label">Tipo Documento:</label>
                <div class="col-md-10">
                    <asp:TextBox ID="txtTipoDocumento" runat="server" CssClass="form-control" value="CC" Enabled="false"></asp:TextBox>                    
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 col-form-label">Cedula:</label>
                <div class="col-md-10">
                    <asp:TextBox ID="txtCedula" runat="server" CssClass="form-control" PlaceHolder="Ingrese Cedula"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCedula" runat="server" ErrorMessage="El numero de cedula es requerida" ControlToValidate="txtCedula" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvCedula" runat="server" ErrorMessage="La cedula debe ser numerico" ControlToValidate="txtCedula" Operator="DataTypeCheck" Type="Integer" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger"></asp:CompareValidator>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 col-form-label">Fecha Nacimiento:</label>
                <div class="col-md-10">
                    <asp:TextBox ID="txtNacimiento" runat="server" CssClass="form-control" PlaceHolder="Dia/Mes/Año"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvFechaNacimiento" runat="server" ErrorMessage="La fecha de nacimiento es requerida" ControlToValidate="txtNacimiento" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revFechaNacimiento" runat="server" ErrorMessage="Fecha invalida" ControlToValidate="txtNacimiento" ForeColor="Red" ValidationExpression="(^((((0[1-9])|([1-2][0-9])|(3[0-1]))|([1-9]))\x2F(((0[1-9])|(1[0-2]))|([1-9]))\x2F(([0-9]{2})|(((19)|([2]([0]{1})))([0-9]{2}))))$)" CssClass="alert-danger"></asp:RegularExpressionValidator>               
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-md-2 col-md-10">
                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-md-2 col-md-10">
                    <asp:Label ID="lbResultado" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
