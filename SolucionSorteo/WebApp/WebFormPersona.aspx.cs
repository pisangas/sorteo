﻿using AccesoDatos;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class WebFormPersona : System.Web.UI.Page
    {
        #region [Variables privadas]
        FachadaPersona _fachadaPersona = new FachadaPersona();
        #endregion

        #region [Metodos privados]
        private void CargarPersona(int cedula)
        {
            try
            {
                Persona persona = _fachadaPersona.RetornarPersona(cedula);
                if (persona != null)
                {
                    var dateTime = persona.F_Nacimiento;
                    txtTipoDocumento.Text = persona.TipoCedula.ToString();
                    txtCedula.Text = persona.NroDocumento.ToString();
                    txtCedula.Enabled = false;
                    txtNombre.Text = persona.Nombres;
                    txtApellidos.Text = persona.Apellidos;
                    txtNacimiento.Text = (dateTime.ToShortDateString());
                    btnGuardar.Text = "Actualizar Persona";
                }
                else
                {
                    lbResultado.CssClass = "text-danger";
                    lbResultado.Text = $"La persona con cedula {cedula} no existe";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region [Eventos de la pagina]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request["NroDocumento"]))
                {
                    CargarPersona(Convert.ToInt32(Request["NroDocumento"]));
                }
            }
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Persona persona = new Persona
                    {
                        Nombres = txtNombre.Text,
                        Apellidos = txtApellidos.Text,
                        TipoCedula = "CC",
                        NroDocumento = Convert.ToInt32(txtCedula.Text),
                        F_Nacimiento = Convert.ToDateTime(txtNacimiento.Text),
                    };
                    if (!txtCedula.Enabled)
                    {
                        if (_fachadaPersona.ActualizarPersona(persona) == 1)
                        {
                            Response.Redirect("/WebFormListarPersonas.aspx");
                        }
                        else
                        {
                            lbResultado.CssClass = "text-danger";
                            lbResultado.Text = $"No se pudo actualizar la persona {persona.Nombres} {persona.Apellidos}";
                        }

                    }
                    else
                    {
                        if (_fachadaPersona.InsertarPersona(persona) == 1)
                        {
                            txtNombre.Text = txtApellidos.Text = txtTipoDocumento.Text = txtCedula.Text = txtNacimiento.Text = "";
                            lbResultado.CssClass = "text-success";
                            lbResultado.Text = "Se agrego exitosamente la persona";
                        }
                        else
                        {
                            lbResultado.CssClass = "text-danger";
                            lbResultado.Text = "Ocurrio un error al ingresar la persona";
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                lbResultado.CssClass = "text-danger";
                lbResultado.Text = ex.Message;
            }
        }
        #endregion
    }
}