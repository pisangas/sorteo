﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormGenerarSorteo.aspx.cs" Inherits="WebApp.WebFormGenerarSorteo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mt-3">
        <div class="card-header text-white badge-dark ">
            Generar Sorteo
        </div>
    </div>
    <div>
        <link href="Content/paginacion.css" rel="stylesheet" />
        <asp:GridView ID="gvPPremios" runat="server" CssClass="table table-striped table-dark table-hover mt-3" AutoGenerateColumns="false" AllowPaging="true" PageSize="5" OnPageIndexChanging="gvPPremios_PageIndexChanging">
            <Columns>
                <asp:BoundField HeaderText="Premio" DataField="IdPremio" />
                <asp:HyperLinkField DataTextField="IdPersona" HeaderText="Numero Identificación" DataNavigateUrlFormatString="/WebFormListarPersonas.aspx?NroDocumento={0}" DataNavigateUrlFields="IdPersona" />
                <asp:BoundField HeaderText="Cantiddad" DataField="Cantidad" />
                <asp:BoundField HeaderText="Fecha Asignacion" DataField="Fecha" />
            </Columns>
        </asp:GridView>
    </div>
    <div class="col-md-7 offset-5 mt-3">
        <asp:Button ID="btnGenerarSorteo" runat="server" Text="Generar Sorteo" CssClass="btn btn-outline-dark" OnClick="btnGenerarSorteo_Click" />
    </div>
    <asp:Label ID="lbResultado" runat="server" Text=""></asp:Label>
</asp:Content>
