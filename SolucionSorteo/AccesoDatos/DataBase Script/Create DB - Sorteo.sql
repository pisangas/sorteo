--*********************************************************************************************************
-- Crear Base de Datos
--*********************************************************************************************************
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = 'DBSorteo')
BEGIN
	CREATE DATABASE DBSorteo;	
END

GO

USE DBSorteo

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Tabla de Personas
-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CREATE TABLE Personas (
  Id INT IDENTITY(1,1) NOT NULL,
  TipoCedula NVARCHAR(2) NOT NULL,
  NroDocumento INT NOT NULL,
  Nombres NVARCHAR(100) NOT NULL,
  Apellidos NVARCHAR(100) NOT NULL,
  F_Nacimiento DATE NOT NULL,  
);

-- Llave primaria
ALTER TABLE Personas ADD CONSTRAINT pk_Personas PRIMARY KEY (NroDocumento);

INSERT INTO Personas (TipoCedula, NroDocumento, Nombres, Apellidos, F_Nacimiento) VALUES ('CC', 35999439, 'LUISA MARIA', 'ORTEGA ORTIZ', '1991-01-12');
INSERT INTO Personas (TipoCedula, NroDocumento, Nombres, Apellidos, F_Nacimiento) VALUES ('CC', 71345999, 'CARLOS MARIO', 'FUENTES MORA', '1988-01-12');

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Tabla de premios
-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CREATE TABLE Premios (
  Codigo INT IDENTITY(1,1) NOT NULL,
  Descripcion NVARCHAR(100) NOT NULL,
  Cantidad INT NOT NULL,  
);

-- Llave primaria
ALTER TABLE Premios ADD CONSTRAINT pk_Premios PRIMARY KEY (Codigo);

INSERT INTO Premios (Descripcion, Cantidad) VALUES ('Balon Blanco', 3);
INSERT INTO Premios (Descripcion, Cantidad) VALUES ('Balon Negro', 1);
INSERT INTO Premios (Descripcion, Cantidad) VALUES ('Dulce Blanco', 2);

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Tabla de PPremios
-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CREATE TABLE PPremios (
  id INT IDENTITY(1,1) NOT NULL,
  IdPremio INT NOT NULL,
  IdPersona INT NOT NULL,
  Fecha DATE NOT NULL,  
  Cantidad INT NOT NULL,
);

-- Llave primaria
ALTER TABLE PPremios ADD CONSTRAINT pk_PPremios PRIMARY KEY (id);

-- Llave foranea de la categoria
ALTER TABLE PPremios ADD CONSTRAINT fk_PPremios_IdPersona FOREIGN KEY (IdPersona) REFERENCES Personas(NroDocumento);
ALTER TABLE PPremios ADD CONSTRAINT fk_PPremios_IdPremio FOREIGN KEY (IdPremio) REFERENCES Premios(Codigo);







